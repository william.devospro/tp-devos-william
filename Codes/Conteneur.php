

<?php
#E:\DEVOIRS\Projet\SiteWeb\public_html\application\controllers\REST

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';  //librairie a inclure pour le service web

class Conteneur extends REST_Controller
{

    public  function __construct()
    {
        parent::__construct();
        $this->load->model('M_Conteneur');
        $this->load->helper("url");
    }

    public function index_get($id = '')
    {
        if ($id == "") {// Si id est vide alors il selectionne toute les informations sur la base de donn�es

            $results = $this->M_Conteneur->select_all();
            $this->response($results, REST_Controller::HTTP_OK);
                                
        } else {// Sinon il selectionne les details d'un conteneur 

            $results = $this->M_Conteneur->select_detail_by_conteneur($id);

            
            if (count($results) != 0) {// Si result est differents de 0, la connexion est bien etablie 

                $this->response($results, REST_Controller::HTTP_OK);
                
            } else {//ERREUR 404

                $this->response($results, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function index_optimisee_get($id = '')
    {
        if ($id == "") {// Si id est vide alors il selectionne toute les informations sur la base de donn�e
            $results = $this->M_Conteneur->select_optimisee();      //utilise la fonction select_optimisee
            $this->response($results, REST_Controller::HTTP_OK);                
        } else {// Sinon il selectionne les details d'un conteneur 
            $results = $this->M_Conteneur->select_optimisee_detail($id);
            if (count($results) != null) {// Si result est differents de null, la connexion est bien etablie 
                $this->response($results, REST_Controller::HTTP_OK);
            } else {//ERREUR 404
                $this->response($results, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function index_delete($id = '')
    {
        if ($id !== '') {

            $results = $this->M_Conteneur->delete($id);
            $this->response($results, REST_Controller::HTTP_CREATED);
        } else {

            $this->response($results, REST_Controller::HTTP_NOT_FOUND);
        }
    }
    #####################################D�but Codage William###########################################
    public function index_put($id =! '')    //R�cup�re l'id dans le dernier segment URL
    {
        
            parse_str(file_get_contents("php://input"), $dto); //R�cup�ration des donn�es re�ues et conversion de celles-ci en string
            $dto = json_encode($dto,true); //Retourne la repr�sentation JSON de $dto
            $dto = preg_replace('#[^0-9a-z]+#i','', $dto); //Retire les caract�res speciaux 
        
            #On prends ce dont on a besoins dans la string
            $dto = substr($dto, -4, 2); // choix de la position : retourne "30,31,32,33 ou 34"
			#########Uniquement pour une requ�te sigfox de cette forme########
			//	{															//
			//		"device":"{device}",									//
			//		"data":"{data}"											//
			//	}															//
			//# Le changement de cette forme entrainera le changement 		//
			//# de position dans la fonction substr()                       //
			##################################################################
			
            switch ($dto != null) { //Si $dto n'est pas vide

                case $dto == 30: //si le taux de remplissage vaut 30
                    $tauxremplissage = "0"; //enregistre "0" (0%) dans la variable local 

                    break;

                case $dto == 31: //si le taux de remplissage vaut 31
                    $tauxremplissage = "1"; //enregistre "1" (25%) dans la variable local 

                    break;

                case $dto == 32: //si le taux de remplissage vaut 32
                    $tauxremplissage = "2"; //enregistre "2" (50%) dans la variable local 
    
                    break;    

                case $dto == 33: //si le taux de remplissage vaut 33
                    $tauxremplissage = "3"; //enregistre "3" (75%) dans la variable local 
    
                break;

                case $dto == 34: //si le taux de remplissage vaut 34
                    $tauxremplissage = "4"; //enregistre "4"(100%) dans la variable local 

                break;

                default:
                    $tauxremplissage = "Erreur 404"; //Retourne Erreur 404 en cas d'erreur
        
            };
            
            $results = $this->M_Conteneur->put($tauxremplissage,$id); // Appel de la fonction put avec comme param�tres le $tauxremplissage et l'$id
            $this->response($results, REST_Controller::HTTP_CREATED); // Connexion �tablie
    } else {

        $this->response($results, REST_Controller::HTTP_NOT_FOUND); // Erreur 404
    }

    ###################################Fin Codage William############################################
}
