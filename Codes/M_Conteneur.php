

<?php
#E:\DEVOIRS\Projet\SiteWeb\public_html\application\models
defined('BASEPATH') or exit('No direct script access allowed');

class M_Conteneur extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
    }

    public function select_all()
    {
        $query = $this->db->select('Id, LatLng')
            ->from('conteneur')
            ->get();
        return $query->result_array();
    }


    public function select_detail_by_conteneur($prmid)
    {
        $query = $this->db->select('Id, LatLng, TourneeStandardId, AddrEmplacement, VolumeMesureActuel')
            ->from('conteneur')
            ->where('TourneeStandardId', $prmid)
            ->get();
        return $query->result_array();
    }

    public function select_search_by_page($search)
    {
        $config_p['reuse_query_string'] = true;
        $query = $this->db->select('Id, AddrEmplacement')
            ->from('conteneur')
            ->like('AddrEmplacement', $search)
            ->get();
        return $query->result_array();
    }

    public function select_optimisee()
    {
        $query = $this->db->select('Id, LatLng, VolumeMesureActuel, AddrEmplacement')
            ->from('conteneur')
            //->where('VolumeMesureActuel',"3")
            ->get();
        return $query->result_array();
    }

    
    public function select_optimisee_detail($prmid)
    {
        $query = $this->db->select('Id, LatLng, VolumeMesureActuel, AddrEmplacement, TourneeStandardId')
            ->from('conteneur')
            ->where('TourneeStandardId', $prmid)
            ->get();
        return $query->result_array();
    }
    ##################################William##############################################################################
    public function put($tauxremplissage,$prmid){  

        $query = "update `conteneur` set `VolumeMesureActuel` = '$tauxremplissage' where `Id` = '$prmid'"; //Requ�te MySql
        $query = $this->db->query($query); //Envoie de la requ�te et stockage de cell-ci dans une varible
        return $query; //On retourne la variable afin de voir l'�tat de l'envoi(utilis� surtout pour POSTMAN)
        
    }

}
