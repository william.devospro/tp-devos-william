    #include <Wire.h>             //inclure la bibliothèque Wire pour I2C
    #include <SoftwareSerial.h>   //inclure la bibliothèque pour Serial et mySerial
    
    #define rxPin D7  //PIN D7 réglée pour la réception
    #define txPin D6  //PIN D6 réglée pour la transmission
    
    int reading = 0;
    int volumeMax = 0;
    int TEMPS_SOMMEIL = 50; //Réglage su sommeil en secondes
    String valeurHexa;
    String ConvertirRequete;

    SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin); // Réglage des PIN (D7, D6) pour mySerial (Émetteur)
   
    void setup()
    {
      Wire.begin();               // rejoindre le bus I2C
      Serial.begin(9600);         // demarrer la communication série du capteur à une vitesse de 9600bps
      mySerial.begin(9600);       // demarrer la communication série de l'émetteur à une vitesse de 9600bps
      pinMode(rxPin, INPUT);      // Configuration de la PIN RX (Reception) en INPUT (entrée)
      pinMode(txPin, OUTPUT);     // Configuration de la PIN TX (Transmission) en OUTPUT (sortie)
    }

    void DeepSleepStart()
    {
      ESP.deepSleep(TEMPS_SOMMEIL * 1000000); // Fonction qui indique le temps de sommeil du mode deepSleep
    }
    

    void loop()
    {
      Serial.println("\nBonjour!"); // Message du réveil de l'ESP
      // etape 1: configuration du capteur
      Wire.beginTransmission(112); // debuter la transmission sur l'hôte #112 (0x70)
      // l'adresse scécifiée dans la documentation est 224 (0xE0)
      // mais l'adressage I2C utilise les 7 plus haut bytes donc c'est 112
      Wire.write(byte(0x00));      // Initialiser l'adresse ou l'on doit écrire à 0 (0x00)
      Wire.write(byte(0x51));      // commande du capteur pour récupérer les "centimetres" (0x51)
      Wire.endTransmission();      // arreter la transmission

      // etape 2: delais d'attente afin que la configuration (etape 1) se passe bien 
      delay(70);                   // delais d'attente de 70 millisecondes

      // etape 3: dire au capteur de renvoyer une valeur bien particulière
      Wire.beginTransmission(112); // demarrer la transmission vers l'hote #112
      Wire.write(byte(0x02));      // débuter l'écriture sur le byte 0x02
      Wire.endTransmission();      // arrêter la transmission

      // etape 4: envoyer la lecture du capteur
      Wire.requestFrom(112, 2);    // envoyer 2 bytes à partir de l'hôte esclave #112

      // step 5: réception de la lecture du capteur
      if (2 <= Wire.available())   // si 2 bytes on bien été reçus
      {
        reading = Wire.read();     // enregistrer la lecture dans une variable
        Serial.print(reading);     // afficher le contenu de "reading"
        Serial.println(" = valeur avant décalage");
        reading = reading << 8;    // décaler les byte de poid fort pour être les 8 bytes de poid fort
        reading |= Wire.read();    // reception des 8 bits de poids le plus faible
        Serial.print(reading);     // afficher le contenu de "reading"
        Serial.println("cm");      // affiche "cm"
      // step 6: Calcul du niveau de remplissage
        if(reading < 50){         // Si la lecture est inférieur à 50
          volumeMax = 4;          // Le niveau remplissage sera de 4
          valeurHexa = "34";      // Stocker la valeur 04 dans valeurHexa
          
        }else if(reading > 50 && reading < 80){  // Si la lecture est supérieure à 50 et inférieur à 80
          volumeMax = 3;          // Le niveau remplissage sera de 3
          valeurHexa = "33";      // Stocker la valeur 03 dans valeurHexa
          
        }else if(reading > 80 && reading < 110){  // Si la lecture est supérieure à 80 et inférieur à 110
          volumeMax = 2;          // Le niveau remplissage sera de 2
          valeurHexa = "32";      // Stocker la valeur 02 dans valeurHexa
          
        }else if(reading > 110 && reading < 140){  // Si la lecture est supérieure à 110 et inférieur à 140
          volumeMax = 1;          // Le niveau remplissage sera de 1
          valeurHexa = "31";      // Stocker la valeur 01 dans valeurHexa
          
        }else if(reading > 140){  // Si la lecture est supérieure à 140
          volumeMax = 0;          // Le niveau remplissage sera de 0
          valeurHexa = "30";      // Stocker la valeur 00 dans valeurHexa
          
        }

      }
      
      ConvertirRequete = "AT$SF=" + valeurHexa + ",1\r";  // Mise en forme de la requête sigfox
      
      mySerial.println();         // Montre l'état dans putty

      const char* requeteSigfox = ConvertirRequete.c_str(); // Permet de récupérer le char contenu dans ConvertirRequete de type string
                                                            // et le stocker dans un pointeur de type char
      
      mySerial.write(requeteSigfox);  // Écriture et envoi de la requête
      
      delay(2000);                // Temporisation d'execution de la boucle avant le début du deepSleep
      
      DeepSleepStart();           // Lancement de la fcontion : L'ESP s'endort
      
    }
    
    
    
